package com.test.view

import android.content.Context
import android.opengl.GLSurfaceView
import android.util.AttributeSet
import com.test.scene.SceneRenderer

class SceneSurfaceView : GLSurfaceView {

    private lateinit var sceneRenderer: SceneRenderer

    constructor(context: Context) : super(context) {
        initRenderer(context)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        initRenderer(context)
    }

    fun releaseRenderer() {
        sceneRenderer.releaseSceneObjects()
    }

    private fun initRenderer(context: Context) {
        setEGLContextClientVersion(GLES_VERSION)
        sceneRenderer = SceneRenderer(context)
        setRenderer(sceneRenderer)
    }

    companion object {
        private const val GLES_VERSION = 2
    }
}