package com.test.scene

class SceneTimer {

    private var startTime: Long = -1
    private var started = false

    fun update(): Double =
        if (started) {
            if (startTime == -1L) {
                startTime = System.nanoTime()
            }
            val deltaTime = System.nanoTime() - startTime
            startTime = System.nanoTime()
            deltaTime / NANOS
        } else {
            0.0
        }

    fun start() {
        startTime = -1
        started = true
    }

    fun stop() {
        started = false
    }

    companion object {
        private const val TAG = "SceneTimer"
        private const val NANOS = 1000000000.0
    }
}