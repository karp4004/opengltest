package com.test.scene

import org.apache.commons.math3.geometry.euclidean.twod.Vector2D

data class Ray2D(
    val start: Vector2D,
    val direction: Vector2D? = null,
    val nextRay: Ray2D? = null
)