package com.test.scene

import android.content.res.Resources
import android.graphics.BitmapFactory
import android.opengl.GLES20
import android.opengl.GLES20.GL_TEXTURE_2D
import android.opengl.GLUtils
import com.test.R
import org.apache.commons.math3.geometry.euclidean.twod.Vector2D

class SceneTexture : SceneObject {

    private val textures = IntArray(1)

    override fun onSurfaceCreated(resources: Resources) {
        loadTexture(resources)
    }

    override fun onSurfaceChanged(viewportProjection: List<Vector2D>) {
    }

    override fun onDrawFrame(mvpMatrix: FloatArray) {
    }

    fun activateTexture() {
        GLES20.glActiveTexture(GLES20.GL_TEXTURE0)
        GLES20.glBindTexture(GL_TEXTURE_2D, textures[0])
    }

    override fun onRelease() {
        GLES20.glDeleteTextures(textures.size, textures, 0)
    }

    private fun loadTexture(resources: Resources) {
        GLES20.glGenTextures(1, textures, 0)
        if (textures[0] == GLES20.GL_FALSE)
            throw RuntimeException("Error loading texture")
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textures[0])
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_NEAREST)
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_NEAREST)
        val bitmap = BitmapFactory.decodeResource(resources, R.drawable.bumpy_bricks_public_domain)
        GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, bitmap, 0)
        bitmap.recycle()
    }
}