package com.test.scene

import android.opengl.GLES20
import java.nio.ByteBuffer
import java.nio.ByteOrder
import java.nio.FloatBuffer
import java.nio.ShortBuffer

class SceneBuffer {

    lateinit var vertexBuffer: FloatBuffer

    private var indicesLength = 0
    private lateinit var drawListBuffer: ShortBuffer

    fun setVertextBuffer(data: FloatArray) {
        vertexBuffer =
            ByteBuffer.allocateDirect(data.size * FLOAT_SIZE).order(ByteOrder.nativeOrder())
                .asFloatBuffer()
        vertexBuffer.put(data)
    }

    fun setIndexBuffer(shortArray: ShortArray) {
        drawListBuffer = ByteBuffer.allocateDirect(shortArray.size * SHORT_SIZE).apply {
            order(ByteOrder.nativeOrder())
        }.asShortBuffer()
        drawListBuffer.put(shortArray)
        drawListBuffer.position(0)
        indicesLength = shortArray.size
    }

    fun draw() {
        GLES20.glDrawElements(
            GLES20.GL_TRIANGLES, indicesLength,
            GLES20.GL_UNSIGNED_SHORT, drawListBuffer
        )
    }

    fun drawLine() {
        GLES20.glDrawElements(
            GLES20.GL_LINES, indicesLength,
            GLES20.GL_UNSIGNED_SHORT, drawListBuffer
        )
    }

    fun releaseBuffer() {
        if (::vertexBuffer.isInitialized) {
            vertexBuffer.clear()
        }
    }

    companion object {
        private const val FLOAT_SIZE = 4
        private const val SHORT_SIZE = 2
    }
}