package com.test.scene

import android.content.res.Resources
import android.opengl.GLES20
import org.apache.commons.math3.geometry.euclidean.twod.Vector2D
import java.nio.FloatBuffer

abstract class SceneShader : SceneObject {

    abstract val vertexSrc: String
    abstract val fragmentSrc: String

    private var vertexHandle = -1
    private var fragmentHandle = -1
    private var programHandle = -1

    private var uMVPMatrix = -1
    private var uTexture = -1
    protected var aPosition = -1
    protected var aTexPos = -1

    override fun onSurfaceCreated(resources: Resources) {
        vertexHandle = loadShader(GLES20.GL_VERTEX_SHADER, vertexSrc)
        fragmentHandle = loadShader(GLES20.GL_FRAGMENT_SHADER, fragmentSrc)
        programHandle = createProgram(vertexHandle, fragmentHandle)

        uMVPMatrix = GLES20.glGetUniformLocation(programHandle, "uMVPMatrix")
        uTexture = GLES20.glGetUniformLocation(programHandle, "uTexture")
        aPosition = GLES20.glGetAttribLocation(programHandle, "aPosition")
        aTexPos = GLES20.glGetAttribLocation(programHandle, "aTexPos")
    }

    override fun onSurfaceChanged(viewportProjection: List<Vector2D>) {
    }

    override fun onDrawFrame(mvpMatrix: FloatArray) {
    }

    fun preRender() {
        GLES20.glUseProgram(programHandle)
    }

    fun setMVPMatrix(mvpMatrix: FloatArray) {
        GLES20.glUniformMatrix4fv(uMVPMatrix, 1, false, mvpMatrix, 0)
    }

    fun setTexture() {
        GLES20.glUniform1i(uTexture, 0)
    }

    abstract fun setVertexBuffer(buffer: FloatBuffer)

    private fun loadShader(shaderType: Int, shaderSource: String): Int {
        val handle = GLES20.glCreateShader(shaderType)
        checkErrorStatus(handle)
        GLES20.glShaderSource(handle, shaderSource)
        GLES20.glCompileShader(handle)
        val compileStatus = IntArray(1)
        GLES20.glGetShaderiv(handle, GLES20.GL_COMPILE_STATUS, compileStatus, 0)
        checkStatus(compileStatus, handle)
        return handle
    }

    private fun createProgram(vertexShader: Int, fragmentShader: Int): Int {
        val handle = GLES20.glCreateProgram()
        checkErrorStatus(handle)
        GLES20.glAttachShader(handle, vertexShader)
        GLES20.glAttachShader(handle, fragmentShader)
        GLES20.glLinkProgram(handle)
        val linkStatus = IntArray(1)
        GLES20.glGetProgramiv(handle, GLES20.GL_LINK_STATUS, linkStatus, 0)
        checkStatus(linkStatus, handle)
        return handle
    }

    private fun checkStatus(status: IntArray, handle: Int) {
        if (status[0] == 0) {
            onRelease()
            throw RuntimeException("Error in program linking: ${GLES20.glGetProgramInfoLog(handle)}")
        }
    }

    private fun checkErrorStatus(handle: Int) {
        if (handle == GLES20.GL_FALSE)
            throw RuntimeException("Error creating program!")
    }

    override fun onRelease() {
        GLES20.glDeleteShader(vertexHandle)
        GLES20.glDeleteShader(fragmentHandle)
        GLES20.glDeleteProgram(programHandle)
    }

    companion object {
        internal const val FLOAT_SIZE = 4
        internal const val POSITION_SIZE = 2
        internal const val TEXTURE_SIZE = 2
        internal const val POSITION_OFFSET = 0
        internal const val TEXTURE_OFFSET = 2
    }
}