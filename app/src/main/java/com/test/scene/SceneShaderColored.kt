package com.test.scene

import android.opengl.GLES20
import java.nio.FloatBuffer

class SceneShaderColored : SceneShader() {
    override val vertexSrc =
        "uniform mat4 uMVPMatrix;\n" +
            "attribute vec2 aPosition;\n" +
            "void main() {\n" +
            "  gl_Position = uMVPMatrix * vec4(aPosition.xy, 0.0, 1.0);\n" +
            "}"

    override val fragmentSrc =
        "precision mediump float;\n" +
            "void main(void)\n" +
            "{\n" +
            "  gl_FragColor = vec4(1.0, 1.0, 0.5, 1.0);\n" +
            "}"

    override fun setVertexBuffer(buffer: FloatBuffer) {
        buffer.position(POSITION_OFFSET)
        GLES20.glVertexAttribPointer(
            aPosition,
            POSITION_SIZE,
            GLES20.GL_FLOAT,
            false,
            POSITION_SIZE * FLOAT_SIZE,
            buffer
        )
        GLES20.glEnableVertexAttribArray(aPosition)
    }
}