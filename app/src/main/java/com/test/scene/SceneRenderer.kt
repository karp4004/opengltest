package com.test.scene

import android.content.Context
import android.opengl.GLES20
import android.opengl.GLSurfaceView
import android.opengl.GLU
import android.opengl.Matrix
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.res.ResourcesCompat
import com.example.android.opengl.Line
import com.example.android.opengl.Square
import com.test.R
import com.test.viewmodel.SquareViewModel
import org.apache.commons.math3.geometry.euclidean.twod.Vector2D
import javax.microedition.khronos.egl.EGLConfig
import javax.microedition.khronos.opengles.GL10

class SceneRenderer(context: Context) : GLSurfaceView.Renderer {
    private val resources = context.resources
    private val viewModel by (context as AppCompatActivity).viewModels<SquareViewModel>()
    private val texturedShader = SceneShaderTextured()
    private val coloredShader = SceneShaderColored()
    private val sceneTexture = SceneTexture()
    private val square =
        Square(texturedShader, sceneTexture, ResourcesCompat.getFloat(resources, R.dimen.square_radius))
    private val line = Line(coloredShader, ResourcesCompat.getFloat(resources, R.dimen.square_radius))

    private val sceneObjects = listOf(
        viewModel, texturedShader, coloredShader, sceneTexture, line, square
    )

    private val mMVPMatrix = FloatArray(16)
    private val mProjectionMatrix = FloatArray(16)
    private val mViewMatrix = FloatArray(16)

    override fun onSurfaceCreated(
        gl: GL10,
        config: EGLConfig
    ) {
        viewModel.setSquare(square)
        sceneObjects.forEach {
            it.onSurfaceCreated(resources)
        }
    }

    override fun onSurfaceChanged(gl: GL10, width: Int, height: Int) {

        val ratio = width.toFloat() / height
        Matrix.frustumM(mProjectionMatrix, 0, -100 * ratio, 100 * ratio, -100f, 100f, 1f, 100f)
        Matrix.setLookAtM(mViewMatrix, 0, 0f, 0f, -1f, 0f, 0f, 0f, 0f, 1.0f, 0.0f)
        Matrix.multiplyMM(mMVPMatrix, 0, mProjectionMatrix, 0, mViewMatrix, 0)

        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT or GLES20.GL_DEPTH_BUFFER_BIT)
        GLES20.glViewport(0, 0, width, height)
        GLES20.glClearColor(0.0f, 0.7f, 0.5f, 1f)
        GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA)
        GLES20.glEnable(GLES20.GL_BLEND)
        GLES20.glLineWidth(10.0f)

        val viewport = IntArray(4)
        GLES20.glGetIntegerv(GLES20.GL_VIEWPORT, viewport, 0)
        val list = listOf(
            getScreenCornerProj(viewport, 0, 3),
            getScreenCornerProj(viewport, 2, 3),
            getScreenCornerProj(viewport, 0, 0),
            getScreenCornerProj(viewport, 2, 0)
        )
        sceneObjects.forEach {
            it.onSurfaceChanged(list)
        }
    }

    override fun onDrawFrame(gl: GL10) {
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT)
        sceneObjects.forEach {
            it.onDrawFrame(mMVPMatrix)
        }
    }

    fun releaseSceneObjects() {
        sceneObjects.forEach {
            it.onRelease()
        }
    }

    private fun getScreenCornerProj(viewport: IntArray, cornerX: Int, cornerY: Int): Vector2D {
        val xy = FloatArray(4)
        GLU.gluUnProject(
            viewport[cornerX].toFloat(),
            viewport[cornerY].toFloat(), 0f, mViewMatrix, 0, mProjectionMatrix, 0, viewport, 0, xy, 0
        )
        return Vector2D(xy[0].toDouble(), xy[1].toDouble())
    }
}